// Read version from package.json
const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')
const version = JSON.parse(packageJson).version || 0

const webpack = require('webpack');
const CopyWebpackPlugin = require("copy-webpack-plugin");

const config = require('./src/assets/config.json');

process.env.VUE_CLI_TEST = false

// Stamp service worker with current version string!
const fileNameIn = "service-worker-source.js";
const fileNameOut = "service-worker-versioned.js";
fs.readFile(fileNameIn, 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    var result = data.replace(/APP_VERSION_PLACEHOLDER/g, "'" + version + "'");

    fs.writeFile(fileNameOut, result, 'utf8', function (err) {
        if (err) return console.log(err);
    });
});

module.exports = {
    devServer: {
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        },
        proxy: {
            "^/localproxy": {
                changeOrigin: true,
                //followRedirects: true,
                target: "localhost",
                router: function (req) {
                    // Remove "/localproxy"
                    var path = req.originalUrl.substring(12);
                    // Host is the first segment
                    path = path.substring(0, path.indexOf("/"));
                    var host = path;
                    return {
                        protocol: 'https:',
                        host: host,
                    }
                },
                pathRewrite: function (path, req) {
                    // Remove the first segment, that is the host
                    var p = path.substring(12);
                    p = p.substring(p.indexOf("/"));
                    return p;
                },
                logLevel: 'debug',
                secure: false
            },
        }
    },
    publicPath: process.env.NODE_ENV === 'production'
        ? './'
        : './',

    pwa: {
        name: config.appName,
        themeColor: "#0E813D",
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black',
        manifestOptions: {
            start_url: "./index.html",
            scope: ".",
            background_color: "#FFFFFF",
            display: "standalone",
            iconPaths: {
                favicon32: 'img/icons/favicon-32x32.png',
                favicon16: 'img/icons/favicon-16x16.png',
                appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
                maskIcon: 'img/icons/safari-pinned-tab.svg',
                msTileImage: 'img/icons/msapplication-icon-144x144.png'
            },
            "icons": [
                {
                    "src": "./img/icons/android-chrome-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/android-chrome-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-60x60.png",
                    "sizes": "60x60",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-76x76.png",
                    "sizes": "76x76",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-120x120.png",
                    "sizes": "120x120",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-152x152.png",
                    "sizes": "152x152",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-180x180.png",
                    "sizes": "180x180",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon.png",
                    "sizes": "180x180",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/favicon-16x16.png",
                    "sizes": "16x16",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/favicon-32x32.png",
                    "sizes": "32x32",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/msapplication-icon-144x144.png",
                    "sizes": "144x144",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/mstile-150x150.png",
                    "sizes": "150x150",
                    "type": "image/png"
                }
            ],
        },
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: './service-worker-versioned.js',
            swDest: 'service-worker.js',
            maximumFileSizeToCacheInBytes: 8000000,
            exclude: [
                'assets/fonts/', 
                /\.map$/,
                /workbox\/workbox-v7.0.0\/(?!(workbox-sw\.js|workbox-routing\.prod\.js|workbox-strategies\.prod\.js|workbox-cacheable-response\.prod\.js|workbox-expiration\.prod\.js|workbox-precaching\.prod\.js)$)/,
                ///workbox\/workbox-v7.0.0\/(?!(workbox-sw\.js|workbox-routing\.dev\.js|workbox-strategies\.dev\.js|workbox-cacheable-response\.dev\.js|workbox-expiration\.dev\.js|workbox-precaching\.dev\.js)$)/,
                'img/icons/',
                'img/splash/',
                'service-worker-debug.js',
                'config.json'
            ]
        }
    },

    configureWebpack: {
        devtool: 'source-map',
        plugins: [
            new webpack.DefinePlugin({
                'process.env.PACKAGE_VERSION': '"' + version + '"'
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: "./src/assets/config.json",
                        to: "./"
                    }
                ]
            }),
        ],
        resolve: {
            fallback: {
                "stream": require.resolve("stream-browserify"), 
                "timers": require.resolve("timers-browserify")
            }
        }
    },

    lintOnSave: 'warning',
}
