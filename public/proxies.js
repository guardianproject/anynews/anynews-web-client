// eslint-disable-next-line
(function () {
    class ProxyHandlerClass {
        constructor() {
            this.proxies = [];
            this.proxiedUrls = [];
            this.idxCurrentProxy = 0;
            this.useStaticProxy = false; // Set to true to replace all URLs in incoming data
            this.setRandomProxy(); // Init to random
        }

        /**
         * The ProxyHandler is considered initialized when the config file has been loaded/set
         */
        isInitialized() {
            return !!this.config;
        }

        setConfig(config) {
            this.config = config;
            this.proxies = config.proxies || [];
            this.proxiedUrls = config.proxiedUrls || [];
            this.setRandomProxy();
        }

        loadConfig() {
            console.log("loadConfig()");
            return fetch('./config.json')
                .then((res) => {
                    return res.json();
                })
                .then(json => {
                    this.setConfig(json);
                    return json;
                })
                .catch(err => {
                    console.error("Failed to get config:", err);
                    throw(err);
                });
        }

        getCurrentProxy = function () {
            if (this.proxies.length == 0 || this.idxCurrentProxy >= this.proxies.length || this.idxCurrentProxy < 0) {
                return null;
            }
            return this.ensureTrailingSlash(this.proxies[this.idxCurrentProxy]);
        }

        setCurrentProxy = function (proxy) {
            if (proxy) {
                this.idxCurrentProxy = this.proxies.indexOf(proxy);
                if (this.idxCurrentProxy >= 0) {
                    return; // success
                }
            }

            // Not set or invlid, use a random one!
            this.setRandomProxy();
        }

        setRandomProxy() {
            this.idxCurrentProxy = Math.floor(Math.random() * Math.floor(this.proxies.length));
        }

        moveToNextProxy = function () {
            if (this.proxies.length > 0) {
                this.idxCurrentProxy = (this.idxCurrentProxy + 1) % this.proxies.length;
                return this.getCurrentProxy();
            }
            return null;
        }

        ensureTrailingSlash(url) {
            if (url.endsWith("/")) {
                return url;
            }
            return url + "/";
        }

        getProxiedUrl(urlString) {
            if (urlString && !urlString.startsWith(".") && !urlString.startsWith("/")) {
                try {
                    const url = new URL(urlString); 
                    for (let p of this.proxiedUrls) {
                        try {
                            let proxyProtocol =  p.split("//")[0];
                            let proxyHost = p.split("://")[1];
      
                            // Get hostname by stripping away path and optional port
                            //
                            let proxyHostname = proxyHost;
                            let idxSlash = proxyHostname.indexOf("/");
                            let idxColon = proxyHostname.indexOf(":");
                            if (idxSlash >= 0) {
                                proxyHostname = proxyHostname.substring(0, idxSlash);
                            }
                            if (idxColon >= 0 && idxColon < idxSlash) {
                                proxyHostname = proxyHostname.substring(0, idxColon);
                            }
      
                            if (url.protocol == proxyProtocol) {
                                const proxyHostParts = proxyHost.split(".");
                                const urlHostnameParts = url.hostname.split(".");
                                if (proxyHostParts.length == urlHostnameParts.length && proxyHostParts.every((part, index) => part == "*" || part == urlHostnameParts[index])) {
                                    const proxy = this.getCurrentProxy();
                                    const urlRewritten = this.ensureTrailingSlash(proxy) + url.hostname + url.pathname + url.search; 
                                    console.log("URL rewrite: " + url + " -> " + urlRewritten);
                                    return urlRewritten;
                                }
                            }
                        }
                        catch (ex) {
                            console.error("Invalid proxy url", p, ex);
                        }
                    }
                }
                catch (ex) {
                  console.warn(ex);
                }
            }
            return urlString;
        }

        /**
         * Replace all proxied URLs with the current proxy. This is used in cases where a service worker is
         * not available.
         * NOTE - Static replace is called ONLY from where we know config is already loaded, so no need to check
         * ProxyHandler.isInitialized() here!!!
         * @param {*} data String with content, containing URLs and other stuff. 
         * @returns Same content but with all URLs that should be proxied replaced with current proxy.
         */
        staticReplace(data) {
            var result = data;
            // From: https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
            var escapeRegExp = function (string) {
                return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
            };
            for (var p of this.proxiedUrls) {
                const proxiedUrl = this.ensureTrailingSlash(p);
                var proxiedUrlRegexp = escapeRegExp(proxiedUrl).replaceAll("\\*", "([^\\.]+?)");
                var regexp = new RegExp(proxiedUrlRegexp, 'gi');
                result = result.replace(regexp, (match, ignoredp1, ignoredp2, ignoredp3, ignoredoffset, ignoredstring) => { return this.getCurrentProxy() + match.split("://")[1] });
            }
            return result;
        }
    }
    var ProxyHandler = new ProxyHandlerClass();
    if (typeof window !== "undefined") {
        window.ProxyHandler = ProxyHandler;
    } else if (typeof self !== "undefined") {
        self.ProxyHandler = ProxyHandler;
    }
}());
