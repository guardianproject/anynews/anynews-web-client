import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import Vue from 'vue'
import Vuetify from 'vuetify/dist/vuetify.min.js'
import 'vuetify/dist/vuetify.min.css'

var icons = {};

// Load ALL icons as components and add to the icons array, that is registered below.
const ComponentContext = require.context('../icons/', true, /\.vue$/i);
ComponentContext.keys().forEach((componentFilePath) => {
    let comp = ComponentContext(componentFilePath).default;
    icons[comp.name] = { component: comp };
});

Vue.use(Vuetify);

const vuetifyInstance = new Vuetify(
  {
    icons: {
      iconfont: 'md',
      values: icons
    },
    options: {
      customProperties: true
    },
    theme: {
      options: {
        customProperties: true,
      },
      dark: false,
      themes: {
        light: {
          primary: '#232323',
          secondary: '#949494',
          accent: '#5577ff',
          cardBackground: '#ffffff',
          audioCardBackground: '#fafafa'
        }
      }
    }
  }
)
vuetifyInstance.onConfigUpdate = (vuetify, config) => {
  vuetify.theme.themes.light.accent = config.accentColor;
};
export default vuetifyInstance;