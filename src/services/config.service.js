export default {
    install(Vue, defaultServerFromLocation, onloaded) {
        var config = Vue.observable(require('@/assets/config.json'));
        const getRuntimeConfig = () => {
            return fetch('./config.json').then((res) => res.json()).catch(err => {
                console.error("Failed to get config:", err);
                return {};
            });
        }

        config.promise = getRuntimeConfig().then((json) => {
            // Reactively use all the config values
            for (const key of Object.keys(json)) {
                Vue.set(config, key, json[key]);
            }

            // Set defaults
            if (config.enableShare === undefined) {
                config.enableShare = true;
            }
            if (config.enableFavorites === undefined) {
                config.enableFavorites = true;
            }
            if (config.enableDownload === undefined) {
                config.enableDownload = true;
            }

            // Tell callback we are done loading runtime config
            if (onloaded) {
                onloaded(config);
            }
            return config;
        });
        Vue.prototype.$config = config;
    }
}
