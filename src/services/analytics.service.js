import EmptyLogger from "../loggers/empty.js";

function getLogger(type, config) {
  var loggers = {};
  function importAllLoggers(r) {
    return r.keys().map((res) => {
      // Remove"./"
      const parts = res.split("/");
      const name = parts[1].split(".")[0];
      loggers[name] = r(res);
    });
  }
  importAllLoggers(require.context("@/loggers/", true, /\.js$/));
  if (loggers[type]) {
    return new loggers[type].default(config);
  }
  return new EmptyLogger();
}

export default {
  install(Vue) {
    const analyticsService = new Vue({
      data() {
        return {
          engines: [],
          cachedEvents: [],
          initialized: false,
        };
      },
      created() {
        const emptyLoggerInstance = new EmptyLogger();
        let proto = Object.getPrototypeOf(emptyLoggerInstance);
        let functions = Object.getOwnPropertyNames(proto).filter(item => typeof proto[item] === 'function' && item !== 'constructor');
        for (const f of functions) {
            this[f] = (args) => {
              if (this.initialized) {
                this.engines.forEach(engine => {
                  engine[f].call(engine, args);
                });
              } else {
                this.cachedEvents.push({f: f, a: args});
              }
            }
        }

        this.$config.promise.then((config) => {
          var analytics = config.analytics || {};
          if (!Array.isArray(analytics)) {
            analytics = [analytics];
          }
          for (const engineConfig of analytics) {
            const engine = engineConfig.enabled ? getLogger(engineConfig.type, engineConfig.config || {}) : null;
            if (engine) {
              engine.$root = this.$root;
              engine.$store = this.$store;
              this.engines.push(engine);
            }
          }

          this.initialized = true;

          // Handle cached events
          this.cachedEvents.forEach(({f, a}) => {
            this.engines.forEach(engine => {
              engine[f].call(engine, a);
            });        
          });
          this.cachedEvents = [];
        });
      },
      methods: {
        setRoot(root, store) {
          this.$root = root;
          this.$store = store;
          this.engines.forEach(engine => {
            engine.$root = engine.$root || root;
            engine.$store = engine.$store || store;
          });   
        }
      }
    });
    Vue.prototype.$logger = analyticsService;
    window.logger = analyticsService;
  },
};
