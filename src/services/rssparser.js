import sanitizeHTML from "sanitize-html";
import mime from 'mime';
import FeedModel from "../models/feedmodel.js";
import ItemModel from "../models/itemmodel.js";
import JSZip from "jszip/dist/jszip.min.js";
import { parseString } from "xml2js";

const ProxyHandler = require("./proxy").default;

export default class RSSParser {
  static fetchUrl(url, basePath, defaultImage, db) {
    const self = this;
    if (!url) {
      return Promise.reject("No URL");
    }

    if (url.startsWith("/") && basePath && !url.startsWith("/localproxy")) {
      url = basePath + url;
    } else if (url.startsWith("./")) {
      var prefix = window.location.origin + window.location.pathname;
      prefix = prefix.substring(0, prefix.lastIndexOf("/"));
      url = prefix + url.substring(1); // Remove the '.'
    }

    if (url.endsWith("zip")) {
      return window.axios.get(url, { responseType: "blob" }).then(function(response) {
        console.log("Loading ZIP file");
        JSZip.loadAsync(response.data).then(function(zip) {
          console.log("Loaded ZIP file");
          var result = null;
          var promises = [];

          zip.forEach(function(relativePath, file) {
            console.log("iterating over", relativePath);
            if (relativePath == "index.rss") {
              promises.push(
                file.async("string").then(function(text) {
                  result = self.parseFeed(self, text, url, defaultImage);
                })
              );
            } else if (relativePath.startsWith("enc/") && !file.dir) {
              let url = "file://" + relativePath;

              // Cached already?
              promises.push(
                db.getMediaFile(url).then(function(blob) {
                  if (blob == null) {
                    promises.push(
                      file.async("nodebuffer").then(function(binaryData) {
                        let blob = new Blob([binaryData], { type: "audio/mpeg" });
                        db.media.add({ url: "file://" + relativePath, blob: blob });
                      })
                    );
                  }
                })
              );
            }
          });

          // Wait until all promises are fulfilled
          Promise.all(promises).then(function() {
            return Promise.resolve({ feed: result.feed, items: result.items.map((item) => { 
              item.feedModel = result.feed;
              return item;
            })});
          });
        });
      });
    } else {
      console.log("Ask axios to get: " + url);
      const options = {
        method: "get",
        url: url,
        //headers: {common: {'x-destination': 'feed'}}
      };
      return window.axios(options).then(function(response) {
        let result = self.parseFeed(self, response.data, url, defaultImage);
        return Promise.resolve({ feed: result.feed, items: result.items.map((item) => { 
          item.feedModel = result.feed;
          return item;
        })});
      });
    }
  }

  static parseFeed(self, data, url, defaultImage) {
    if (ProxyHandler.useStaticProxy) {
      // Replace all URLs that need to be proxied.
      data = ProxyHandler.staticReplace(data);
    }

    var parseResult = null;
    parseString(data, { explicitArray: false }, function(ignoredError, result) {
      //console.log(result);
      if (result["rdf:RDF"] != null) {
        parseResult = self.parseRDF(self, result, url, defaultImage);
      } else if (result.rss != null) {
        parseResult = self.parseRSS(self, result, url, defaultImage);
      }
    });
    return parseResult;
  }

  static parseRSS(self, result, feedUrl, feedDefaultImage) {
    return self.parseData(self, result.rss.channel, result.rss.channel, feedUrl, feedDefaultImage);
  }

  static parseRDF(self, result, feedUrl, feedDefaultImage) {
    return self.parseData(self, result["rdf:RDF"].channel, result["rdf:RDF"], feedUrl, feedDefaultImage);
  }

  static getText = function(elt) {
    if (typeof elt === "string") return elt;
    if (typeof elt === "object" && Object.prototype.hasOwnProperty.call(elt, "_")) return elt._;
    return null;
  };

  static relativeToAbsolute(feedUrl, url) {
    if (feedUrl && url && url.startsWith("./")) {
      if (feedUrl.startsWith("/localproxy")) {
        const result = new URL(url, feedUrl.replace("/localproxy", "https:/")).href;
        return result.replace("https:/", "/localproxy");
      }

      // from: https://stackoverflow.com/questions/14780350/convert-relative-path-to-absolute-using-javascript
      return new URL(url, feedUrl).href;
    } else {
      return url;
    }
  }

  static parseData(self, channelElement, itemParentElement, feedUrl, feedDefaultImage) {
    var items = [];

    var feed = new FeedModel();
    feed.url = feedUrl;
    feed.title = channelElement.title;
    feed.link = channelElement.link;
    feed.language = channelElement.language;
    feed.description = channelElement.description;
    if (channelElement.image != null) {
      if (channelElement.image.$ != null && channelElement.image.$["rdf:resource"] != null) {
        feed.imageUrl = channelElement.image.$["rdf:resource"];
      } else {
        feed.imageUrl = channelElement.image.url;
      }
    }
    if (feed.imageUrl == null && channelElement["itunes:image"] != null) {
      feed.imageUrl = channelElement["itunes:image"].$.href;
    }

    // Categories
    var feedCategories = channelElement["itunes:category"];
    if (feedCategories != null) {
      if (Array.isArray(feedCategories) && feedCategories.length > 0) {
        feed.category = feedCategories[0].$.text;
      } else {
        feed.category = feedCategories.$.text;
      }
    }
    if (feed.category == null) {
      feedCategories = channelElement["category"];
      if (feedCategories != null) {
        if (Array.isArray(feedCategories) && feedCategories.length > 0) {
          feed.category = self.getText(feedCategories[0]);
        } else {
          feed.category = self.getText(feedCategories);
        }
      }
    }

    // Make array if not already
    if (!Array.isArray(itemParentElement.item)) {
      if (!itemParentElement.item) {
        itemParentElement.item = [];
      } else {
        itemParentElement.item = [itemParentElement.item];
      }
    }

    itemParentElement.item.forEach((i) => {
      const item = self.parseItem(self, i, feedUrl, feedDefaultImage);
      items.push(item);
    });

    // Should not be needed, but here we filter out duplicate guid:s from the items (or else
    // the lists will break because they use guids as :key and that has to be unique)
    const uniqueItems = [];
    const map = new Map();
    for (const item of items) {
      if (!map.has(item.guid)) {
        map.set(item.guid, true); // set any value to Map
        uniqueItems.push(item);
      } else {
        console.log("Ignoring item: " + item.title);
      }
    }
    return { feed: feed, items: uniqueItems };
  }

  static parseItemString(data) {
    if (ProxyHandler.useStaticProxy) {
      // Replace all URLs that need to be proxied.
      data = ProxyHandler.staticReplace(data);
    }

    let item = null;
    parseString(data, { explicitArray: false }, (ignoredError, result) => {
      if (result && result.item) {
        item = RSSParser.parseItem(this, result.item, null, null);
      }
    });
    return item;
  }

  static parseItem(self, i, feedUrl, feedDefaultImage) {
    var item = new ItemModel();
    item.feed = feedUrl;
    item.title = i.title;
    item.link = self.getText(i.link);
    item.guid = self.getText(i.guid);
    if (item.guid == null || item.guid.length == 0) {
      item.guid = item.title;
    }
    //console.log(item.guid);
    item.description = sanitizeHTML(i.description);
    item.pubDate = i.pubDate;
    if (item.pubDate == null) {
      item.pubDate = self.getText(i["dc:date"]);
    }
    item.author = i.author;
    item.content = sanitizeHTML(self.getText(i["content:encoded"]) || self.getText(i["content"]));

    var mediaContent = i["media:content"];
    if (!mediaContent && i["media:group"]) {
      // Try to get media from first media group!
      var mediaGroup = i["media:group"];
      if (Array.isArray(mediaGroup) && mediaGroup.length > 0) {
        mediaContent = mediaGroup[0]["media:content"];
      } else if (mediaGroup != null) {
        mediaContent = mediaGroup["media:content"];
      }
    }

    const setEnclosure = (enclosure, enclosureType, thumb) => {
      if (item.enclosure == null || item.enclosure.length == 0) {
        item.enclosure = enclosure;
        item.enclosureType = enclosureType;
        if (thumb && !item.imageSrc) {
          item.imageSrc = thumb.$.url;
        }
      }
      if (item.imageSrc == null && enclosure && enclosureType && enclosureType.startsWith("image/")) {
        item.imageSrc = enclosure;
      }
    }

    const handleMediaContent = (mediaContent) => {
      const medium = mediaContent.$.medium;
      const type = mediaContent.$.type;
      const url = mediaContent.$.url;
      const thumb = mediaContent["media:thumbnail"];
      if (medium == "video" || (!medium && (type || mime.getType(url) || "").startsWith("video/"))) {
        setEnclosure(url, type || mime.getType(url) || "video/mp4", thumb);
      } else if (medium == "audio" || (!medium && (type || mime.getType(url) || "").startsWith("audio/"))) {
        setEnclosure(url, type || mime.getType(url) || "audio/mp3", thumb);
      } else if (item.imageSrc == null) {
        item.imageSrc = mediaContent.$.url;
      }
    };

    if (Array.isArray(mediaContent) && mediaContent.length > 0) {
      for (var idxMC = 0; idxMC < mediaContent.length; idxMC++) {
        handleMediaContent(mediaContent[idxMC]);
      }
    } else if (mediaContent != null) {
      handleMediaContent(mediaContent);
    }

    var enclosure = i["enclosure"];
    if (Array.isArray(enclosure) && enclosure.length > 0) {
      setEnclosure(enclosure[0].$.url, enclosure[0].$.type);
    } else if (enclosure != null) {
      setEnclosure(enclosure.$.url, enclosure.$.type);
    }

    // Try RDF enclosure
    if (item.enclosure == null || item.enclosure.length == 0) {
      enclosure = i["enc:enclosure"];
      if (Array.isArray(enclosure) && enclosure.length > 0) {
        setEnclosure(enclosure[0].$["rdf:resource"], enclosure[0].$.type)
      } else if (enclosure != null) {
        setEnclosure(enclosure.$["rdf:resource"], enclosure.$.type);
      }
    }

    // Try to figure out category from dc:subject.
    var categories = i["dc:subject"];
    if (categories == null) {
      categories = i["category"];
    }
    if (categories != null) {
      if (Array.isArray(categories) && categories.length > 0) {
        item.category = self.getText(categories[0]);
        item.allCategories = categories.map((c) => self.getText(c));
      } else if (categories != null) {
        item.category = self.getText(categories);
        item.allCategories = [item.category];
      }
    }

    // Keywords
    var tags = i["keywords"];
    if (tags != null) {
      if (Array.isArray(tags) && tags.length > 0) {
        item.tags = self.getText(tags[0]);
      } else if (tags != null) {
        item.tags = self.getText(tags);
      }
    }

    // Handle superfeed extensions
    if (i["superfeed:channelTitle"] != null) {
      item.channelTitle = i["superfeed:channelTitle"];

      // Use channel title as category
      item.category = item.channelTitle;
    }
    if (i["superfeed:channelDescription"] != null) {
      item.channelDescription = i["superfeed:channelDescription"];
    }
    if (i["itunes:image"] != null && !item.imageSrc) {
      item.imageSrc = i["itunes:image"].$.href;
      console.log("Set image to", item.imageSrc);
    }

    // Set image from enclosure?
    if (item.imageSrc == null && item.enclosure && item.enclosureType && item.enclosureType.startsWith("image/")) {
      item.imageSrc = item.enclosure;
    } else if (item.imageSrc == null && item.content != null) {
      //Try to find an image in the content
      parseString(
        "<root>" + item.content + "</root>",
        {
          explicitRoot: true,
          explicitArray: true,
          strict: true,
        },
        function(err, result) {
          console.log("Parsed");
          if (err == null && result != null) {
            var image = result.root.img == null ? null : result.root.img[0];
            if (image != null) {
              item.imageSrc = image.$.src;

              // Now that we use the first image as "header image", remove that one from the content!
              console.log("Remove old image!!!" + item.imageSrc);
              item.content = item.content.replace(/<img[^>]*>/i, "<span></span>");
            }
          }
        }
      );
    }

    // Still no image? Use default feed one if set.
    if (!item.imageSrc) {
      item.imageSrc = feedDefaultImage;
    }

    if (item.content != null) {
      // Remove "width" and "height" from youtube embeds and wrap them in a
      // <div class="videoWrapper">
      item.content = item.content.replace(
        /(<iframe[^>]+)height=".*?"([^>]*>.*<\/iframe>)/i,
        "<div class='videoWrapper'>$1$2</div>"
      );
      item.content = item.content.replace(/(<iframe[^>]+)width=".*?"/i, "$1");
    }

    // Transform all relative URL:s into absolute ones, since they are relative to RSS, not the PWA.
    //
    for (const key of Object.keys(item)) {
      const o = item[key];
      if (o && typeof o === "string" && o.startsWith("./")) {
        const absolute = self.relativeToAbsolute(feedUrl, o);
        console.log("Found relative: " + o + " for key name " + key + " -> " + absolute);
        item[key] = absolute;
      }
    }
    return item;
  }
}
