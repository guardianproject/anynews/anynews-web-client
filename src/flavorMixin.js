export default {
  methods: {
    currentFlavor() {
      let flavor = this.$config.flavors[this.$store.state.flavor];
      if (!flavor) {
        flavor = this.$config.flavors["default"];
      }
      if (!flavor && Object.keys(this.$config.flavors).length > 0) {
        flavor = this.$config.flavors[Object.keys(this.$config.flavors)[0]];
      }
      return flavor || {};
    },
  }
}