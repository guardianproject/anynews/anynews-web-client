import Vue from "vue";
import Router from "vue-router";
import Main from "./views/Main.vue";
import store from "./store";

Vue.use(Router);

var childRoutes = [
  {
    path: "",
    name: "home",
    component: () => import("./views/Home.vue"),
    props: true,
    meta: { tab: 0 },
  },
  {
    path: "/more",
    name: "more",
    component: () => import("./views/More.vue"),
    props: true,
    meta: { tab: 4 },
  },
  {
    path: "/item/:service/:guid",
    name: "item",
    component: () => import("./views/Home.vue"),
    props: true,
    meta: { tab: 0 },
    redirect: (to) => {
      const name = to.params.service;
      const item = to.params.guid;
      if (name) {
        const service = routerInstance.config ? routerInstance.config.flavors[name] : null;
        if (service && item) {
          store.commit("setFlavor", name);
          store.state.onboarded = true;
          store.commit("setCurrentItemLink", item);
          return { name: "home" }
        }
      }
      return "/";
    },
  },
];

const routerInstance = new Router({
  routes: [
    {
      path: "/",
      name: "root",
      component: Main,
      children: childRoutes,
    },
    {
      path: "/flavor/:name?",
      redirect: (to) => {
        const name = to.params.name;
        if (name) {
          const service = routerInstance.config ? routerInstance.config.flavors[name] : null;
          if (service) {
            console.log("Selecting flavor by param:", name);
            store.commit("setFlavor", name);
            store.state.onboarded = true;

            if ("static" in to.query) {
              console.log("Flavor selection is static!");
              store.commit("setFlavorStatic", true);
            }
          }
        }
        return "/";
      },
    },
    {
      path: "/onboarding",
      name: "onboarding",
      component: () => import("./views/Onboarding.vue"),
    },
    { path: '*', redirect: "/" }
  ],
});
routerInstance.onConfigUpdate = (config) => {
  
  routerInstance.config = config;
  if (config.enableCategories) {
    routerInstance.addRoute("root",{
      path: "/categories",
      name: "categories",
      component: () => import("./views/Home.vue"),
      props: { headerType: "categories" },
      meta: { tab: 1 },
    });

  } else {
    // TODO - If we UPDATE the config at some point, and remove category support, we might end up here
    // with a route already added, so need to call "routerInstance.removeRoute("categories");", but that API is
    // not available in Vue Router 3.x that we currently use, so ignoring this edge case for now.
  }

  if (config.enableFavorites) {
    routerInstance.addRoute("root", {
      path: "/saved",
      name: "saved",
      component: () => import("./views/Home.vue"),
      props: { headerType: "saved" },
      meta: { tab: 3 },
    });
  } else {
    // TODO - If we UPDATE the config at some point, and remove favorite support, we might end up here
    // with a route already added, so need to call "routerInstance.removeRoute("saved");", but that API is
    // not available in Vue Router 3.x that we currently use, so ignoring this edge case for now.
  }
};
export default routerInstance;
