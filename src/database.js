import Dexie from "dexie";

export function createDB(keyname) {
  const db = new Dexie(keyname);

  // IF YOU CHANGE HERE, DO IT IN SERVICE WORKER CODE AS WELL!!!
  db.version(1).stores({
    media: "url",
    items: "id",
  });
  db.version(2).stores({
    feeds: "url",
  });
  db.version(3).stores({
    items: "id, feed",
  });
  db.version(4).stores({
    log_sw: "++id",
  });

  db.getMediaFile = async function (url) {
    if (url == null) {
      return Promise.resolve(null);
    }
    return await db.media.get(url);
  };

  db.getSWLogs = async function () {
    return await db.log_sw.toArray();
  };

  db.clearSWLogs = async function () {
    return await db.log_sw.clear();
  };
  return db;
}
