export default {
  computed: {
    logotype() {
      if (this.$config.logo) {
        return this.$config.logo;
      }
      return require("/public/img/logo.svg");
    },
    logotypeInverted() {
      if (this.$config.logoInverted) {
        return this.$config.logoInverted;
      }
      return require("/public/img/logoInverted.svg");
    }
  }
}