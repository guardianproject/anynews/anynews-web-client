import BaseLogger from "./empty";
import moment from "moment";

var queuedTealiumStates = [];
var queuedTealiumActions = [];

// Initialize Tealium analytics
window.utag_cfg_ovrd = window.utag_cfg_ovrd || {}; //Make sure we don't kill a previous decleration of the object
window.utag_cfg_ovrd.noview = true;

function onUtagLoaded(config) {
  window.utag.cfg.datasource = config.datasource;

  // Send queued events
  for (var i = 0; i < queuedTealiumStates.length; i++) {
    //console.log("Send queued event: ");
    window.utag.view(queuedTealiumStates[i]); //, function() { utag.DB("Queued page view has been tracked"); });
  }
  queuedTealiumStates = [];
  for (i = 0; i < queuedTealiumActions.length; i++) {
    //console.log("Send queued event: ");
    window.utag.link(queuedTealiumActions[i]); //, function() { utag.DB("Queued page view has been tracked"); });
  }
  queuedTealiumActions = [];
}

export default class TealiumLogger extends BaseLogger {
  constructor(config) {
    super();

    this.$config = config;

    // Check that all log functions are implemented!
    let thisProto = Object.getPrototypeOf(this);
    let parentProto = Object.getPrototypeOf(thisProto);
    let thisFunctions = Object.getOwnPropertyNames(thisProto).filter(
      (item) => typeof this[item] === "function" && item !== "constructor"
    );
    let parentFunctions = Object.getOwnPropertyNames(parentProto).filter(
      (item) => typeof this[item] === "function" && item !== "constructor"
    );
    for (const f of parentFunctions) {
      if (!thisFunctions.includes(f)) {
        console.warn("Error: log function not implemented: " + f + "!!!");
      }
    }

    // append tealium tag and include a check so that we know when utag is loaded and ready for use in other scripts
    (function(a, b, c, d) {
      a = config.utag;
      b = document;
      c = "script";
      d = b.createElement(c);
      d.src = a;
      d.type = "text/java" + c;
      d.async = true;
      a = b.getElementsByTagName(c)[0];
      a.parentNode.insertBefore(d, a);

      // uses onload, for modern browsers
      d.onload = function() {
        if (!d.onloadDone) {
          d.onloadDone = true;
          onUtagLoaded(config);
        }
      };

      // uses onreadystatechange, for IE9
      d.onreadystatechange = function() {
        if (("loaded" === d.readyState || "complete" === d.readyState) && !d.onloadDone) {
          d.onloadDone = true;
          onUtagLoaded(config);
        }
      };
    })();
  }

  getLogObject() {
    var event = {}; //this.getEmptyLogObject();
    event.property_id = this.$config.propertyId;
    event.rsid_acct = this.$config.rsidAcct;
    event.platform_type = this.$config.platformType;
    event.entity = this.$config.entity;
    event.app_type = this.$config.appType;

    const url = window.location.origin + (this.$root && this.$root.$route ? this.$root.$route.fullPath : "");
    event.canonical_url = url;

    // Do this after the above, since we use those fields!
    event.app_id = event.entity + " " + event.app_type + " : " + this.$root.appVersionString;
    var langInfo = this.languageInfo();
    Object.assign(event, langInfo);
    return event;
  }

  languageInfo() {
    let flavor = this.$store.state.flavor;
    const flavorConfig = this.$config.flavors && this.$config.flavors[flavor];
    return flavorConfig || {};
  }

  logStateOnboardingScreen() {
    var event = this.getLogObject();
    event.page_name = "app onboarding";
    event.content_type = "app onboarding";
    event.subcontent_type = "app onboarding";
    event.section = "app onboarding";
    event.page_title = "app onboarding";
    this.logStateEvent(event);
  }

  logStateHomeScreen() {
    var event = this.getLogObject();
    event.page_name = "home screen";
    event.content_type = "index";
    event.subcontent_type = "homepage";
    event.section = "homepage";
    event.page_title = "home screen";
    this.logStateEvent(event);
  }

  logStateCategoriesScreen() {
    // Get default category name
    let cat = this.$store.state.currentFeedCategories[0];
    let category = cat.category || "default";

    var event = this.getLogObject();
    event.page_name = category;
    event.content_type = "index";
    event.subcontent_type = "section";
    event.section = category;
    event.page_title = category;
    this.logStateEvent(event);
  }

  logStateCategorySelected(category) {
    var event = this.getLogObject();
    event.page_name = category;
    event.content_type = "categories";
    event.subcontent_type = category;
    event.section = category;
    event.page_title = category;
    this.logStateEvent(event);
  }

  logStateSettingsScreen() {
    var event = this.getLogObject();
    event.page_name = "settings";
    event.content_type = "settings";
    event.subcontent_type = "settings";
    event.section = "settings";
    event.page_title = "settings";
    this.logStateEvent(event);
  }

  logStateSavedScreen(selectedTimeFrame) {
    var timeFrame = selectedTimeFrame || "saved";
    if (timeFrame == "week") {
      timeFrame = "this week";
    } else if (timeFrame == "month") {
      timeFrame = "this month";
    }
    var event = this.getLogObject();
    event.page_name = timeFrame;
    event.content_type = "bookmarks";
    event.subcontent_type = "bookmarks";
    event.section = timeFrame;
    event.page_title = timeFrame;
    this.logStateEvent(event);
  }

  logStateEvent(event) {
    if (window.utag !== undefined) {
      window.utag.view(event);
    } else {
      // Queue for sending later, when loaded
      queuedTealiumStates.push(event);
    }
  }

  logActionEvent(event) {
    delete event.page_title;
    if (window.utag !== undefined) {
      window.utag.link(event);
    } else {
      // Queue for sending later, when loaded
      queuedTealiumActions.push(event);
    }
  }

  logPageView(pageName) {
    switch (pageName) {
      case "onboarding":
        this.logStateOnboardingScreen();
        break;
      case "home":
        this.logStateHomeScreen();
        break;
      case "categories":
        this.logStateCategoriesScreen();
        break;
      case "more":
        this.logStateSettingsScreen();
        break;
      case "saved":
        this.logStateSavedScreen();
        break;
    }
  }

  logHeaderTagSelected(tag) {
    if (tag && tag.value) {
      if (tag.value.startsWith("cat_")) {
        this.logStateCategorySelected(tag.name);
      } else if (tag.value.startsWith("saved_")) {
        this.logStateSavedScreen(tag.value.substr(6));
      }
    }
  }

  setArticleProperties(event, item, includePageTitle) {
    event.page_name = item.title;
    event.section = item.getCategoryName();
    if (includePageTitle) {
      event.page_title = item.title;
    }
    if (item.author != null && item.author != "") {
      event.byline = item.author;
    } else {
      // Default to language service
      event.byline = event.language_service;
    }

    let date = moment.utc(item.pubDate);
    event.pub_date = date.format("MM/DD/YYYY");
    event.pub_hour = date.format("HH");
    if (item.description != null && item.description != "") {
      event.slug = item.description;
    }
    event.article_uid = item.guid;
    if (item.tags != null && item.tags != "") {
      event.tags = item.tags;
    }
  }

  logArticleView(item) {
    var event = this.getLogObject();
    let type = "article";
    if (!item.content && item.hasVideoAttachment()) {
      type = "video";
    } else if (!item.content && item.hasAudioAttachment()) {
      type = "audio";
    }
    event.content_type = type;
    event.subcontent_type = type;
    this.setArticleProperties(event, item, true);
    this.logStateEvent(event);
  }

  logVideoListView() {
    var event = this.getLogObject();
    event.page_name = "video detail index";
    event.content_type = "index";
    event.subcontent_type = "video detail index";
    event.section = "video detail";
    event.page_title = "video detail index";
    this.logStateEvent(event);
  }

  logAudioListView() {
    var event = this.getLogObject();
    event.page_name = "audio detail index";
    event.content_type = "index";
    event.subcontent_type = "audio detail index";
    event.section = "audio detail";
    event.page_title = "audio detail index";
    this.logStateEvent(event);
  }

  // Actions
  //

  logAppFirstLoad() {
    var event = this.getLogObject();
    event.KEY = "app_first_launch";
    event.page_name = "app first-launch";
    event.content_type = "app behavior";
    event.subcontent_type = "app behavior";
    event.section = "app behavior";
    event.app_events = "app_first_launch";
    this.logActionEvent(event);
  }

  logAppLoad() {
    var event = this.getLogObject();
    event.KEY = "app_launch";
    event.page_name = "app launch";
    event.content_type = "app behavior";
    event.subcontent_type = "app behavior";
    event.section = "app behavior";
    event.app_events = "app_launch";
    this.logActionEvent(event);
  }

  logAppExit() {
    // var event = this.getLogObject();
    // event.KEY = "app_exit";
    // event.page_name = "app exit";
    // event.content_type = "app behavior";
    // event.subcontent_type = "app behavior";
    // event.section = "app behavior";
    // event.app_events = "app_exit";
    // this.logActionEvent(event);
  }

  logAppError() {
    var event = this.getLogObject();
    event.KEY = "app_crash";
    event.page_name = "app error";
    event.content_type = "app behavior";
    event.subcontent_type = "app behavior";
    event.section = "app behavior";
    event.app_events = "app_crash";
    this.logActionEvent(event);
  }

  logAppUpdated() {
    var event = this.getLogObject();
    event.KEY = "app_update";
    event.page_name = "app update";
    event.content_type = "app behavior";
    event.subcontent_type = "app behavior";
    event.section = "app behavior";
    event.app_events = "app_update";
    this.logActionEvent(event);
  }

  logFetchError(url) {
    if (url === undefined || url == null || url == "") {
      return; // ignore empty
    }
    var event = this.getLogObject();
    event.KEY = "content_not_found";
    event.page_name = "content load error";
    event.content_type = "load error";
    event.subcontent_type = "load error";
    event.section = "content error";
    event.app_events = "content_not_found";

    event.canonical_url = url; // Is this correct?
    this.logActionEvent(event);
  }

  logLanguageSelect(language, fromSettings) {
    var event = this.getLogObject();
    event.KEY = "language_fav";
    var type = fromSettings ? "settings" : "app onboarding";
    event.page_name = type;
    event.content_type = type;
    event.subcontent_type = type;
    event.section = type;
    event.app_events = language;
    this.logActionEvent(event);
  }

  logArticleFavorited(item, type) {
    var event = this.getLogObject();
    this.setArticleProperties(event, item, false);
    event.content_type = type;
    event.subcontent_type = type;
    if (type == "audio") {
      event.KEY = "media_bookmarked";
      event.app_events = "media_bookmarked";
      event.media_type = "ondemand audio";
    } else if (type == "video") {
      event.KEY = "media_bookmarked";
      event.app_events = "media_bookmarked";
      event.media_type = "ondemand video";
    } else {
      event.KEY = "article_to_fav";
      event.app_events = "article_to_fav";
    }
    this.logActionEvent(event);
  }

  logArticleDownload(item, type) {
    var event = this.getLogObject();
    this.setArticleProperties(event, item, false);
    event.content_type = type;
    event.subcontent_type = type;
    if (type == "audio") {
      event.KEY = "download_start";
      event.audio_name = item.title;
      event.media_type = "ondemand audio";
      event.app_events = "download_media";
    } else if (type == "video") {
      event.KEY = "download_start";
      event.video_name = item.title;
      event.media_type = "ondemand video";
      event.app_events = "download_media";
    } else {
      event.KEY = "download_start";
      event.app_events = "download_article";
    }
    this.logActionEvent(event);
  }

  logArticleShare(item, type) {
    var event = this.getLogObject();
    this.setArticleProperties(event, item, false);
    event.content_type = type;
    event.subcontent_type = type;
    event.KEY = "social_share";
    if (type == "audio") {
      event.audio_name = item.title;
      event.app_events = "download_media";
    } else if (type == "video") {
      event.video_name = item.title;
      event.app_events = "download_media";
    } else {
      event.app_events = "download_article";
    }
    this.logActionEvent(event);
  }

  logTextSizeSelect(newSize) {
    var event = this.getLogObject();
    event.KEY = "text_size";
    event.page_name = "settings";
    event.content_type = "settings";
    event.subcontent_type = "settings";
    event.section = "settings";
    event.app_events = "text_size_" + newSize;
    this.logActionEvent(event);
  }

  logClosePlayer(item, type) {
    if (item == null) {
      return;
    }
    var event = this.getLogObject();
    event.KEY = "player_x";
    event.page_name = "player close";
    event.content_type = type;
    event.subcontent_type = type;
    event.section = item.getCategoryName();
    if (type == "audio") {
      event.audio_name = item.title;
      event.media_type = "ondemand audio";
    } else if (type == "video") {
      event.video_name = item.title;
      event.media_type = "ondemand video";
    }
    event.app_events = "player_x";
    this.logActionEvent(event);
  }

  logNavigation(pageName) {
    var event = this.getLogObject();
    switch (pageName) {
      case "home":
        event.KEY = "home_nav";
        event.content_type = "home";
        event.subcontent_type = "home";
        event.page_name = "home nav";
        event.app_events = "home_nav";
        break;
      case "categories":
        event.KEY = "category_nav";
        event.content_type = "categories";
        event.subcontent_type = "categories";
        event.page_name = "category nav";
        event.app_events = "category_nav";
        break;
      case "radio":
        event.KEY = "audio_nav";
        event.content_type = "radio";
        event.subcontent_type = "radio";
        event.page_name = "audio nav";
        event.app_events = "audio_nav";
        break;
      case "more":
        event.KEY = "settings_nav";
        event.content_type = "settings";
        event.subcontent_type = "settings";
        event.page_name = "settings nav";
        event.app_events = "settings_nav";
        break;
      default:
        return;
    }
    this.logActionEvent(event);
  }

  /**
   * eventType: "play", "pause" or "complete"
   */
  logMediaEvent(item, eventTypeVideo, eventTypeAudio) {
    if (item == null || (!item.hasVideoAttachment() && !item.hasAudioAttachment())) {
      return;
    }
    let isVideo = item.hasVideoAttachment();
    var event = this.getLogObject();
    event.page_name = item.title;
    //event.page_title = item.title;
    event.me_content_type = isVideo ? "video" : "audio";
    event.subcontent_type = isVideo ? "video" : "audio";
    event.section = item.getCategoryName();
    if (isVideo) {
      event.KEY = eventTypeVideo;
      event.video_name = item.title;
      event.media_type = item.isLive ? "live video stream" : "ondemand video";
      event.app_events = eventTypeVideo;
      event.video_length = parseInt(item.enclosureDuration || 0, 10);
    } else {
      event.KEY = eventTypeAudio;
      event.audio_name = item.title;
      event.media_type = item.isLive ? "live audio stream" : "ondemand audio";
      event.app_events = eventTypeAudio;
      event.audio_length = parseInt(item.enclosureDuration || 0, 10);
    }
    this.logActionEvent(event);
  }

  logMediaPlay(item) {
    this.logMediaEvent(item, "video_play", "audio_play");
  }

  logMediaPause(item) {
    this.logMediaEvent(item, "video_pause", "audio_pause");
  }

  logMediaComplete(item) {
    this.logMediaEvent(item, "video_complete", "audio_complete");
  }

  logMediaMilestone(item, percent) {
    if (percent != 10 && percent != 50 && percent != 100) return;
    this.logMediaEvent(item, `media_milestone_${percent}_video`, `media_milestone_${percent}_audio`);
  }
}
