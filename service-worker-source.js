/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("./dexie.js");
importScripts("./proxies.js");
importScripts("./workbox/workbox-v7.0.0/workbox-sw.js");

const CONFIG_CACHE = `config`;

const configCacheFiles = ['config.json'];


let db = undefined;

workbox.setConfig({
  debug: false,
  modulePathPrefix: "./workbox/workbox-v7.0.0/",
});

// Load modules
workbox.loadModule("workbox-cacheable-response");
workbox.loadModule("workbox-strategies");
workbox.loadModule("workbox-expiration");
workbox.loadModule("workbox-routing");
workbox.loadModule("workbox-precaching");

self.addEventListener("activate", (event) => {
  event.waitUntil(clients.claim());
});



// on install we download the routes we want to cache for offline
self.addEventListener('install', evt =>
  evt.waitUntil(
    caches.open(CONFIG_CACHE).then(cache => {
      console.log("Caching config files");
      return cache.addAll(configCacheFiles);
    })
  )
);

console.log("Registering static routes");

// Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.
workbox.routing.registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "google-fonts-stylesheets",
  })
);

// Cache the underlying font files with a cache-first strategy for 1 year...
workbox.routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  new workbox.strategies.CacheFirst({
    cacheName: "google-fonts-webfonts",
    plugins: [
      new workbox.cacheableResponse.CacheableResponsePlugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  })
);

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__WB_MANIFEST || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

const fromNetwork = (request, timeout) =>
  new Promise((fulfill, reject) => {
    const timeoutId = setTimeout(reject, timeout);
    fetch(request).then((response) => {
      clearTimeout(timeoutId);
      caches
        .open(CONFIG_CACHE)
        .then((cache) => cache.put(request, response.clone()))
        .then(() => fulfill(response));
    }, reject);
  });

const fromCache = (request) => caches.open(CONFIG_CACHE).then((cache) => cache.match(request));

const configRequest = "config.json";
const loadConfig = fromNetwork(configRequest, 10000).catch(() => fromCache(configRequest)).then((response) => response.json())

const tryLoadConfig = () => {
  console.log("SW make sure config is loaded");
  return loadConfig
    .then((config) => {
      ProxyHandler.setConfig(config);

    console.log("SW has config loaded");
    // Create DB using name
    db = new Dexie(config.appName);
    db.version(1).stores({
      media: "url",
      items: "id",
    });
    db.version(2).stores({
      feeds: "url",
    });
    db.version(3).stores({
      items: "id, feed",
    });
    db.version(4).stores({
      log_sw: "++id",
    });

    if (config.networkFirst) {
      console.log("Feed strategy set to 'network first'");
      workbox.routing.registerRoute(
        new RegExp("https?://.*\\.(xml|rss)"),
        new workbox.strategies.NetworkFirst({
          cacheName: "feed-cache",
          plugins: [
            new workbox.cacheableResponse.CacheableResponsePlugin({
              statuses: [0, 200],
            }),
            new workbox.expiration.ExpirationPlugin({
              maxAgeSeconds: 60 * 60 * 24 * 7, // A week
              maxEntries: 20,
            }),
            proxyCachePlugin,
          ],
        })
      );
    } else {
      console.log("Feed strategy set to 'stale while revalidate'");
      workbox.routing.registerRoute(
        new RegExp("https?://.*\\.(xml|rss)"),
        new workbox.strategies.StaleWhileRevalidate({
          cacheName: "feed-cache",
          plugins: [
            new workbox.cacheableResponse.CacheableResponsePlugin({
              statuses: [0, 200],
            }),
            new workbox.expiration.ExpirationPlugin({
              maxAgeSeconds: 60 * 60 * 24 * 7, // A week
              maxEntries: 20,
            }),
            proxyCachePlugin,
          ],
        })
      );
    }
  })
};

tryLoadConfig().catch((error) => console.error("Initial config load failed:", error));

function cloneRequest(urlOrReq, newUrl, options) {
  // make sure we deal with a Request object even if we got a URL string
  const req = urlOrReq instanceof Request ? urlOrReq : new Request(urlOrReq);

  var init = {};
  Object.keys(Request.prototype).forEach(function (value) {
    init[value] = req[value];
  });
  delete init.url;

  return new Request(newUrl, Object.assign(init, options || {}));
}

function getProxiedRequest(request) {
  const url = request.url.toString();
  const rewrittenUrl = ProxyHandler.getProxiedUrl(url);
  if (url !== rewrittenUrl) {
    return cloneRequest(request, rewrittenUrl);
  }
  return request;
}

const proxyCachePlugin = {
  cacheKeyWillBeUsed: async ({ request, mode }) => {
    console.log("cacheKeyWillBeUsed", request.url);
    return request;
  },
  requestWillFetch: async ({ request }) => {
    // Maybe send request through a proxy
    return getProxiedRequest(request);
  },
};

self.addEventListener("message", (event) => {
  console.log("SW - Got message:");
  console.log(event);
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  } else if (event.data && event.data.type === "GET_VERSION") {
    event.ports[0].postMessage(APP_VERSION_PLACEHOLDER);
  }
});

self.addEventListener("fetch", (e) => {
  const doRequest = (e) => {
    var request = getProxiedRequest(e.request);
    return fetch(request)
      .then((response) => {
        if (response.ok || response.status == 0) return response;
        db.log_sw.add({ type: "fetch_error", url: e.request.url });
        return response;
      })
      .catch((error) => {
        console.error("EXCEPTION FETCHING: ", error, e.request.url);
        throw error;
      });
  };
  if (e.request.url.endsWith("config.json")) {
    e.respondWith(fromNetwork(configRequest, 10000).catch(() => fromCache(configRequest)));
    return;
  };
  const responsePromise = ProxyHandler.isInitialized() ? doRequest(e) : tryLoadConfig().then(() => doRequest(e));
  e.respondWith(
    responsePromise.catch(() => {
      const myOptions = { status: 500, statusText: "Internal error" };
      return new Response(null, myOptions);
    })
  );
});
